import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;

public class mainForm extends JFrame {

    private JPanel panel1;
    private JDateChooser fieldInstructionDate;
    private JButton btnConvert;
    private JComboBox cmbInstructionAt;
    private JComboBox cmbproduct;
    private static JFrame parent;

    private static Properties prop;

    private static SimpleDateFormat format;

    private static String instructionDate;
    private static String instructionAt;
    private static String product;

    private static Long startTime;

    public mainForm()
    {
        parent= new JFrame();
        add(panel1);

        setTitle("Converter CAW OLEOD-DCC V1.0");
        setSize(700,350);

        getConfigFile();

        fieldInstructionDate.setDateFormatString("yyyy/MM/dd");
        JTextFieldDateEditor editor = (JTextFieldDateEditor) fieldInstructionDate.getDateEditor();
        editor.setEditable(false);
        format = new SimpleDateFormat("yyyyMMdd");

        cmbInstructionAt.addItem("0700");
        cmbInstructionAt.addItem("1000");
        cmbInstructionAt.addItem("1300");
        cmbInstructionAt.addItem("1600");
        cmbInstructionAt.setSelectedItem(prop.getProperty("app.instruction_at"));

        cmbproduct.addItem("HYR");
        cmbproduct.addItem("HAC");
        cmbproduct.setSelectedItem("HYR");

        btnConvert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnConvert.setEnabled(false);
                startTime= null;
                startTime = System.nanoTime();
                boolean status = true;

                instructionAt = (String) cmbInstructionAt.getSelectedItem();
                if(fieldInstructionDate.getDate()!=null) {
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
                    DateTimeFormatter dtfTime= DateTimeFormatter.ofPattern("HHmm");
                    LocalDateTime now = LocalDateTime.now();
                    String timeNow= dtfTime.format(now);
                    if (format.format(fieldInstructionDate.getDate()).compareTo(dtf.format(now))==0){
                        if (timeNow.compareTo(instructionAt)>0){
                            status= false;
                            JOptionPane.showMessageDialog(parent, "Instruction At (set in configuration file) has been passed");
                        }
                    }
                    if (format.format(fieldInstructionDate.getDate()).compareTo(dtf.format(now))<0){
                        status= false;
                        JOptionPane.showMessageDialog(parent, "Instruction Date must be empty or equal or greater than today.");
                    }
                    instructionDate = format.format(fieldInstructionDate.getDate());
                }else{
                    instructionAt="";
                }

                product= (String) cmbproduct.getSelectedItem();
                if (status==true) {
                    convert();
                }
                btnConvert.setEnabled(true);
            }
        });

    }

    private void convert() {
        File dir= new File(prop.getProperty("app.folder_in"));
        String[] fileList= dir.list();

        if (fileList.length==0){
            JOptionPane.showMessageDialog(parent, "File conversion error!! Please check the input file.");
        }

        int recordCount;
        for (String nameFile: fileList){
            recordCount=0;
            try {
                FileReader fileReader = new FileReader(prop.getProperty("app.folder_in")+nameFile);
                CSVReader csvReader = new CSVReader(fileReader);
                List<String[]> records = csvReader.readAll();

                String[] out= nameFile.split("\\.");
                File file = new File(prop.getProperty("app.folder_out")+out[0]+"_converted.csv");
                //menghasilkan output di folder OUT sesuai nama folder IN
                FileWriter outputfile = new FileWriter(file);
                CSVWriter writer = new CSVWriter(outputfile, ',', '\u0000', '\u0000', "\n");

                writer.writeNext(new String[]{"H", "", "", "D", "Y", "", instructionDate, instructionAt, ""});

                for (String[] record:records){
                    recordCount+=1;
                    String chargeInstruction= prop.getProperty("app.REM/BEN");
                    if (!record[7].equals("")){
                        if (record[7].equals("o")){
                            chargeInstruction="REM";
                        }else {
                            chargeInstruction = "BEN";
                        }
                    }
                    String debitCharge= prop.getProperty("app.S/C");
                    if (!record[8].equals("")){
                        debitCharge=record[8].toUpperCase();
                    }
                    writer.writeNext(new String[]{"D", product, "", record[0], record[4], "", "", "", "", record[1], "", record[2],
                            "", "", "",record[6], "", record[5], "", "", "", record[3], "", "", chargeInstruction, debitCharge, "", "", "", "",
                            "Y", "", "", "", "", "", "", "", ""});
                }

                long endTime   = System.nanoTime();
                long totalTime = endTime - startTime;
                float sec= totalTime/100000000;
                int seconds= (int)sec;
                JOptionPane.showMessageDialog(parent, "Successfully converting "+recordCount+" record(s) \n in "+seconds+" seconds");

                writer.close();
            } catch (IOException | CsvException e) {
                JOptionPane.showMessageDialog(parent, "File conversion error!! Please check the input file.");
                e.printStackTrace();
            }

        }
    }

    private void getConfigFile() {
        prop= new Properties();
        String properties= "app.config";

        InputStream config = null;
        try {
            config = new FileInputStream(properties);
            prop.load(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createUIComponents() {
        fieldInstructionDate = new JDateChooser();
    }
}
